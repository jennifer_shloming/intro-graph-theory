---
pagetitle: "Exponential Smoothing"
---

```{r setup, echo=FALSE}
source("timeseries.R")
library("statplosion")


spreadsheet_exp_smooth<- function(demand, forecast) {
    months <- 1:15
    columns <- list(months, demand, forecast)
    names(columns) <- c("Months", "Demand", "Forecast")
    create_spreadsheet_with_data(columns, size=c(16,6))
}

plot_exp_smooth_time_series <- function(demand, forecast) {
    names <- c("Demand", "Forecast")
    series <- cbind(ts(demand, start=1), 
                    ts(forecast, start=1))
    plot_periods(series, 
                 type="o", 
                 labels = c("Months", "Units"),
                 xlims=c(1,15), 
                 ylims=c(0,200), 
                 names=names) 
}
```

```{css, eval=T}
table.table {
    width: auto;
}
table {
	  margin: auto;
	  border-top: 3px solid #666;
	  border-bottom: 3px solid #666;
	  margin-bottom: 20px;
}
table thead th { 
border-bottom: 3px solid #ddd; 
}
th, td { 
    	width: auto;
	border: 1px solid #666;
	padding: 5px; 
	height: 30px;
	min-width: 50px;
}
thead, tfoot, tr:nth-child(even) { 
	background: #eee; 
}
```

```{r, eval=F}
```

# Exponential Smoothing 

Next we will talk about a way of forecasting called **Exponential Smoothing** that is very useful, and involves taking a **weighted average** of the **last demand** and the **last forecast**. Many forecasting systems use this technique, and we will see it has a lot of nice properties.

First we have to talk about what weighted averages are, since our technique will involve a weighted average of the demand and the forecast.

## Weighted Averages

If you have two quantities A and B and you want to "mix" them together,
you can form a weighted average of those two quantities. This allows you
to mix them in a more general way than just a 50-50 mix of the two
numbers.

**Example: (Weighted Average of Two Values A and B)**

  | Weighted Average     |  How to compute |  Result is |
  |----------------------|-----------------|------------|
  |50% of A and 50% of B | $.50A + .50B$   |  Half between A and B |
  |25% of A and 75% of B | $.25A + .75B$   |  Closer to B |
  |80% of A and 20% of B | $.80A + .20B$   |  Closer to A |

Notice in each case above the "weights" add up to 100%. This is needed
for a weighted average.

## Weighted Average of Demand and Forecast

The technique we want to look at next for forecasting uses weighted averages of the last demand and the last forecast.

Each month the next forecast is a weighted average of the last demand and the last forecast. 

- if the weight put on the last demand is $\alpha$ 
- then the weight put on the last forecast would be $1-\alpha$ 

For example: 

- if the weight put on the last demand is $0.2$ 
- then the weight put on the last forecast would be $0.8$ 

This allows us to mix together the last demand and last forecast with an adjustable parameter called $\alpha$ that we can change.  

$$
\text{Next Forecast} = (\alpha)(\text{Last Demand}) + (1-\alpha)(\text{Last Forecast})
$$

- when $\alpha$ is close to 1, this is mostly the last demand  
- when $\alpha$ is close to 0, this is mostly the last forecast  

We can see the effects of this weighting later on in some graphs of the demand and forecasts.

## Demand for Month 1 

Suppose our time series starts with month 1 and we only have demand data for that month so far. 

Period 2 has not occured yet. We go ahead and enter the forecast in month 1 as well to be the same as the demand in month 1. This is just a way to start our formula, and is not really a forecast, since we only are in month 1. So here is how we start: 

```{r, fig.align="center", eval=T}
demand <- c(50)
forecast <-c(50)
spreadsheet_exp_smooth(demand, forecast)
```

## Forecast for Month 2

The forecasts will start in month 2 based on the month 1 demand that we have so far and the forecast in month 1 as well.    

For this example we will use $\alpha=0.2$ and so $1-\alpha=0.8$. This means we are weighting the last demand less heavily than the last forecast. We will see the effects of this kind of weighting once we look at some graphs of demand and the forecasts later.   

For now let's look at the calculation for the Month 2 forecast. 

The general formula is this:

\begin{equation}
\text{Next Forecast} = (\alpha)(\text{Last Demand}) + (1-\alpha)(\text{Last Forecast})
\end{equation}

For month 2 this looks like this:

\begin{equation}
\begin{split}
\text{Forecast Month 2} & = (0.2)(\text{Demand Month 1}) + (0.8)(\text{Forecast Month 1}) \\
                      & = (0.2)(50) + (0.8)(50) \\ 
                      & = 50
\end{split}
\end{equation}

So we look at the demand for month 1 and we look at the forecast for month 1 and then we form our forecast for month 2: 

```{r, fig.align="center", eval=T}
demand <- c(50)
forecast <-c(50, 50)
spreadsheet_exp_smooth(demand, forecast)
```

## Forecast for Month 3 

Now once the demand in month 2 arrives, we have this:

```{r, fig.align="center", eval=T}
demand <- c(50, 58)
forecast <-c(50, 50)
spreadsheet_exp_smooth(demand, forecast)
```

and so we forecast for month 3 as follows:

\begin{equation}
\begin{split}
\text{Next Forecast} & = (0.2)(\text{Prev Demand}) + (0.8)(\text{Prev Forecast}) \\
                      & = (0.2)(58) + (0.8)(50) \\ 
                      & = 51.6
\end{split}
\end{equation}

So now we have this:

```{r, fig.align="center", eval=T}
demand <- c(50, 58)
forecast <-c(50, 50, 51.6)
spreadsheet_exp_smooth(demand, forecast)
```

We continue in this fashion each time forecasting the next month from the last months demand and forecast.

In the spreadsheet we will see some properties of this method and see the graph of this technique as well.

## What Is The Effect Of Changing $\alpha$?

Below we show some examples so you can see what effect the $\alpha$ has on this technique:

### Exponential Smoothing $\alpha=0.0$

```{r, fig.align="center", eval=T}
demand<-c(50.00,58.00,98.00,67.00,65.00,55.00,89.00,50.00,68.00,160.00,170.00,180.00,150.00,130.00)
forecast<-c(50.00,50.00,50.00,50.00,50.00,50.00,50.00,50.00,50.00,50.00,50.00,50.00,50.00,50.00,50.00)
plot_exp_smooth_time_series(demand, forecast)
```

When $\alpha=0.0$ that just means that 

\begin{equation}
\begin{split}
\text{Next Forecast} & = (0.0)(\text{Prev Demand}) + (1.0)(\text{Prev Forecast}) \\              
                     & =  (\text{Prev Forecast})
\end{split}
\end{equation}

So that if the first forecast is 50, its just always 50

### Exponential Smoothing  $\alpha=0.1$

```{r, fig.align="center", eval=T}
demand<-c(50.00,58.00,98.00,67.00,65.00,55.00,89.00,50.00,68.00,160.00,170.00,180.00,150.00,130.00)
forecast<-c(50.00,50.00,50.80,55.52,56.67,57.50,57.25,60.43,59.38,60.25,70.22,80.20,90.18,96.16,99.54)
plot_exp_smooth_time_series(demand, forecast)
```

Now it takes a little more of the shape of the Demand.

### Exponential Smoothing  $\alpha=0.2$

```{r, fig.align="center", eval=T}
demand<-c(50.00,58.00,98.00,67.00,65.00,55.00,89.00,50.00,68.00,160.00,170.00,180.00,150.00,130.00)
forecast<-c(50.00,50.00,51.60,60.88,62.10,62.68,61.15,66.72,63.37,64.30,83.44,100.75,116.60,123.28,124.62)
plot_exp_smooth_time_series(demand, forecast)
```

And more and more...

### Exponential Smoothing  $\alpha=0.3$

```{r, fig.align="center", eval=T}
demand<-c(50.00,58.00,98.00,67.00,65.00,55.00,89.00,50.00,68.00,160.00,170.00,180.00,150.00,130.00)
forecast<-c(50.00,50.00,52.40,66.08,66.36,65.95,62.66,70.57,64.40,65.48,93.83,116.68,135.68,139.98,136.98)
plot_exp_smooth_time_series(demand, forecast)
```

### Exponential Smoothing  $\alpha=0.5$

```{r, fig.align="center", eval=T}
demand<-c(50.00,58.00,98.00,67.00,65.00,55.00,89.00,50.00,68.00,160.00,170.00,180.00,150.00,130.00)
forecast<-c(50.00,50.00,54.00,76.00,71.50,68.25,61.63,75.31,62.66,65.33,112.66,141.33,160.67,155.33,142.67)
plot_exp_smooth_time_series(demand, forecast)
```

### Exponential Smoothing  $\alpha=0.7$

```{r, fig.align="center", eval=T}
demand<-c(50.00,58.00,98.00,67.00,65.00,55.00,89.00,50.00,68.00,160.00,170.00,180.00,150.00,130.00)
forecast<-c(50.00,50.00,55.60,85.28,72.48,67.25,58.67,79.90,58.97,65.29,131.59,158.48,173.54,157.06,138.12)
plot_exp_smooth_time_series(demand, forecast)
```

### Exponential Smoothing  $\alpha=0.9$

```{r, fig.align="center", eval=T}
demand<-c(50.00,58.00,98.00,67.00,65.00,55.00,89.00,50.00,68.00,160.00,170.00,180.00,150.00,130.00)
forecast<-c(50.00,50.00,57.20,93.92,69.69,65.47,56.05,85.70,53.57,66.56,150.66,168.07,178.81,152.88,132.29)
plot_exp_smooth_time_series(demand, forecast)
```

### Exponential Smoothing  $\alpha=1.0$

```{r, fig.align="center", eval=T}
demand<-c(50.00,58.00,98.00,67.00,65.00,55.00,89.00,50.00,68.00,160.00,170.00,180.00,150.00,130.00)
forecast<-c(50.00,50.00,58.00,98.00,67.00,65.00,55.00,89.00,50.00,68.00,160.00,170.00,180.00,150.00,130.00)
plot_exp_smooth_time_series(demand, forecast)
```

Until finally it is trying to match the demand as well as it can.

Since $\alpha=1.0$ that just means that 

\begin{equation}
\begin{split}
\text{Next Forecast} & = (1.0)(\text{Prev Demand}) + (0.0)(\text{Prev Forecast}) \\              
                     & =  (\text{Prev Demand})
\end{split}
\end{equation}

So that the next forecast is the prev demand and this is the same as the "naive forecast" that we saw some sections ago.

The "naive forecast" always just predicts next what last happened.

