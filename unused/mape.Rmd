---
pagetitle: "Mape"
---

```{r setup, echo=FALSE}
source("timeseries.R")
library("statplosion")
```

```{css, eval=T}
table.table {
    width: auto;
}
table {
	  margin: auto;
	  border-top: 3px solid #666;
	  border-bottom: 3px solid #666;
	  margin-bottom: 20px;
}
table thead th { 
border-bottom: 3px solid #ddd; 
}
th, td { 
    	width: auto;
	border: 1px solid #666;
	padding: 5px; 
	height: 30px;
	min-width: 50px;
}
thead, tfoot, tr:nth-child(even) { 
	background: #eee; 
}
```

```{r, eval=F}
```

# Measing Error For A Forecast 

## Mean Absolute Percentage Error (MAPE)

Since there are many ways of coming up with a forecast when we are doing time series analysis, how will we determine which one is best?   
     
We need a way to determine how good each forecast is for a particular demand.     
     
There are several ways to do this, we will usually use a method called the **mean absolute percentage error**, or **MAPE**. MAPE is very popular especially for sku demand time series, which is our main application.    

## Lower MAPE Means More Accurate Forecast 

The idea is that the lower the MAPE, the better the forecast fits the demand. If the MAPE is 0% then the forecast lies exactly on top of the demand.

This is the ideal situation. Your forecasting technique predicts exactly correct every single time.

Then the larger the MAPE, the worse the forecast fits the actual demand.

## Choose the Lower MAPE When Comparing Forecasts 

If you have to choose between two forecasts as to which is better, take the one with the lower MAPE.

## Computing the MAPE 

Start with the Demand and Forecast like this in a spreadsheet:

|  |A     | B      | C        | D     | E      | F |
|--|------|--------|----------|-------|--------|---|
|1 |Month | Demand | Forecast |       |        |   |
|2 |1     | 40     |          |       |        |   |
|3 |2     | 35     | 32       |       |        |   |
|4 |3     | 80     | 75       |       |        |   |
|5 |4     | 65     | 70       |       |        |   |
|6 |5     | 45     | 42       |       |        |   |
|7 |6     | 65     | 45       |       |        |   |
|8 |7     |        | 50       |       |        |   |
|9 |      |        |          |       |        |   |
|10|      |        |          |       |        |   |
|11|      |        |          |       |        |   |


## Add error and %error columns 

Create columns **error** and **%error**

|  |A     | B      | C        | D     | E      | F |
|--|------|--------|----------|-------|--------|---|
|1 |Month | Demand | Forecast | error | %error |   |
|2 |1     | 40     |          |       |        |   |
|3 |2     | 35     | 32       |       |        |   |
|4 |3     | 80     | 75       |       |        |   |
|5 |4     | 65     | 70       |       |        |   |
|6 |5     | 45     | 42       |       |        |   |
|7 |6     | 65     | 45       |       |        |   |
|8 |7     |        | 50       |       |        |   |
|9 |      |        |          |       |        |   |
|10|      |        |          |       |        |   |
|11|      |        |          |       |        |   |

## Only Use Months Where Both Demand and Forecast Are Present 

For each month where you have both a Demand and a Forecast you should create and error and a %error that you will use to compute the MAPE.

That means for this example you will have errors and % errors in months 2 through 6.

You will NOT have errors and % errors in month 1 or month 7 since you do not have both a demand and a forecast in those months. You have to exclude those from the MAPE calculation.

In every other month you can calculate the error and %error.

## Calculate Error 

In **D3** enter the formula:  

**=ABS(B3-C3)**

Select the drag handle of this cell and drag it down to get the formulas in cells **D3:D7**:

|  |A      | B      | C        | D           | E               | F |
|--|-------|--------|----------|-------------|-----------------|---|
|1 | Month | Demand | Forecast | error       | %error          |   |
|2 | 1     | 40     |          |             |                 |   |
|3 | 2     | 35     | 32       | =ABS(B3-C3) |                 |   |
|4 | 3     | 80     | 75       | =ABS(B4-C4) |                 |   |
|5 | 4     | 65     | 70       | =ABS(B5-C5) |                 |   |
|6 | 5     | 45     | 42       | =ABS(B6-C6) |                 |   |
|7 | 6     | 65     | 45       | =ABS(B7-C7) |                 |   |
|8 | 7     |        | 50       |             |                 |   |
|9 | 8     |        |          |             |                 |   |
|10|       |        |          |             |                 |   |
|11|       |        |          |             |                 |   |


When this is done you should have this in column D:  

|  |A     | B      | C        | D     | E      | F |
|--|------|--------|----------|-------|--------|---|
|1 |Month | Demand | Forecast | error | %error |   |
|2 |1     | 40     |          |       |        |   |
|3 |2     | 35     | 32       | 3     |        |   |
|4 |3     | 80     | 75       | 5     |        |   |
|5 |4     | 65     | 70       | 5     |        |   |
|6 |5     | 45     | 42       | 3     |        |   |
|7 |6     | 65     | 45       | 20    |        |   |
|8 |7     |        | 50       |       |        |   |
|9 |      |        |          |       |        |   |
|10|      |        |          |       |        |   |
|11|      |        |          |       |        |   |

## Calculate %Error 

In **E3** enter the formula:  

**=D3/B3**

Select the drag handle of this cell and drag it down to get the formulas in cells **E3:E7**:

|  |A      | B      | C        | D           | E               | F |
|--|-------|--------|----------|-------------|-----------------|---|
|1 | Month | Demand | Forecast | error       | %error          |   |
|2 | 1     | 40     |          |             |                 |   |
|3 | 2     | 35     | 32       | 3           | =D3/B3          |   |
|4 | 3     | 80     | 75       | 5           | =D4/B4          |   |
|5 | 4     | 65     | 70       | 5           | =D5/B5          |   |
|6 | 5     | 45     | 42       | 3           | =D6/B6          |   |
|7 | 6     | 65     | 45       | 20          | =D7/B7          |   |
|8 | 7     |        | 50       |             |                 |   |
|9 | 8     |        |          |             |                 |   |
|10|       |        |          |             |                 |   |
|11|       |        |          |             |                 |   |


When this is done you should have this in column E:  

|  |A     | B      | C        | D     | E      | F |
|--|------|--------|----------|-------|--------|---|
|1 |Month | Demand | Forecast | error | %error |   |
|2 |1     | 40     |          |       |        |   |
|3 |2     | 35     | 32       | 3     | 0.09   |   |
|4 |3     | 80     | 75       | 5     | 0.06   |   |
|5 |4     | 65     | 70       | 5     | 0.08   |   |
|6 |5     | 45     | 42       | 3     | 0.07   |   |
|7 |6     | 65     | 45       | 20    | 0.31   |   |
|8 |7     |        | 50       |       |        |   |
|9 |      |        |          |       |        |   |
|10|      |        |          |       |        |   |
|11|      |        |          |       |        |   |

## Interpreting the %Error 

Now we can interpret the results:

- For month 2 the forecast was 9% off from the demand
- For month 3 the forecast was 6% off from the demand
- For month 4 the forecast was 8% off from the demand
- For month 5 the forecast was 7% off from the demand
- For month 6 the forecast was 31% off from the demand

The best forecast for these months was month 3 and the worst forecast for these months was month 6.

## Find the MAPE 

The average of all the %errors is the MAPE.

Compute it by taking the average of E3:E7, the five %errors that you have

|  |A     | B      | C        | D     | E      | F |
|--|------|--------|----------|-------|--------|---|
|1 |Month | Demand | Forecast | error | %error |   |
|2 |1     | 40     |          |       |        |   |
|3 |2     | 35     | 32       | 3     | 0.09   |   |
|4 |3     | 80     | 75       | 5     | 0.06   |   |
|5 |4     | 65     | 70       | 5     | 0.08   |   |
|6 |5     | 45     | 42       | 3     | 0.07   |   |
|7 |6     | 65     | 45       | 20    | 0.31   |   |
|8 |7     |        | 50       |       |        |   |
|9 |      |        |          |       |        |   |
|10|      |        |          |MAPE   |=AVERAGE(E3:E7)|   |
|11|      |        |          |       |        |   |

The result is this:

|  |A     | B      | C        | D     | E      | F |
|--|------|--------|----------|-------|--------|---|
|1 |Month | Demand | Forecast | error | %error |   |
|2 |1     | 40     |          |       |        |   |
|3 |2     | 35     | 32       | 3     | 0.09   |   |
|4 |3     | 80     | 75       | 5     | 0.06   |   |
|5 |4     | 65     | 70       | 5     | 0.08   |   |
|6 |5     | 45     | 42       | 3     | 0.07   |   |
|7 |6     | 65     | 45       | 20    | 0.31   |   |
|8 |7     |        | 50       |       |        |   |
|9 |      |        |          |       |        |   |
|10|      |        |          |MAPE   | 0.12   |   |
|11|      |        |          |       |        |   |

Hence the MAPE for this forecast is 12%.

## Interpret the MAPE 

We can say that on the average the forecast is off from the demand by about 12% of the demand.

