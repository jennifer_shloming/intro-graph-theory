#!/bin/bash

#python -m SimpleHTTPServer 8000 &>/dev/null &

# This runs a reloadable server, watching for html changes 
Rscript -e "servr::httw(pattern='*.html')"

pid=$!

trap ctrl_c EXIT 
function ctrl_c() {
    echo "Killing the servr::httw"
    kill "${pid}"
}

# This runs entr which watches Rmds for changes and recompiles 
# echo "calling ls *.Rmd | entr make"
# ls *.Rmd | entr make
