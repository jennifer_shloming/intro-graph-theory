SHELL:=/bin/bash
SOURCES=$(shell find . -maxdepth 1 -name "*.Rmd" -not -name "index.Rmd" -not -name "appendix.Rmd")
#SOURCES=what-is-statistics.Rmd

HTML_FILES = $(SOURCES:%.Rmd=_book/%.html)

all: _book/index.html $(HTML_FILES)

clean :
	rm -rf _book/* _bookdown_files intro-statistics-bookdown.rds intro-statistics-bookdown.md

_book/index.html : index.Rmd
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::gitbook')"

_book/%.html : %.Rmd
	Rscript -e "bookdown::preview_chapter('$<')"

book :
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::gitbook')"

tufte :
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::tufte_html_book')"

pdf :
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::pdf_book')"

html2 :
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::html_document2')"
	cp intro-statistics-bookdown.html _book

pdf2 :
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::pdf_document2')"

bs4 :
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::bs4_book')"

watch :
	while true; do ls *.Rmd | entr make -j1 ; done

.PHONY: all clean book
